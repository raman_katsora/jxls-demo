package command;

import lombok.Getter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jxls.area.Area;
import org.jxls.command.AbstractCommand;
import org.jxls.command.Command;
import org.jxls.common.CellRef;
import org.jxls.common.Context;
import org.jxls.common.Size;
import org.jxls.transform.poi.PoiTransformer;

public abstract class AbstractDemoCommand extends AbstractCommand {

    @Getter
    private Area area;
    @Getter
    private XSSFSheet sheet;
    @Getter
    private XSSFWorkbook workbook;

    /**
     * @see Command#applyAt(CellRef, Context)
     */
    protected abstract void applyAt(CellRef cellRef, Context context, Size resultSize);

    @Override
    public Size applyAt(CellRef cellRef, Context context) {
        Size resultSize = area.applyAt(cellRef, context);
        if (Size.ZERO_SIZE.equals(resultSize)) {
            return resultSize;
        }

        PoiTransformer transformer = (PoiTransformer) getTransformer();
        workbook = (XSSFWorkbook) transformer.getWorkbook();
        sheet = workbook.getSheet(cellRef.getSheetName());

        applyAt(cellRef, context, resultSize);

        return resultSize;
    }

    @Override
    public Command addArea(Area area) {
        super.addArea(area);
        this.area = area;
        return this;
    }
}
