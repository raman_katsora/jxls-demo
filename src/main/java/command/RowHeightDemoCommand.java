package command;

import lombok.Getter;
import lombok.Setter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.jxls.common.CellRef;
import org.jxls.common.Context;
import org.jxls.common.Size;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RowHeightDemoCommand extends AbstractDemoCommand {

    /**
     * Command name.
     */
    public static final String COMMAND_NAME = "setRowHeight";
    private static final Logger LOGGER = LoggerFactory.getLogger(RowHeightDemoCommand.class);
    @Getter
    @Setter
    private String height;

    @Override
    public String getName() {
        return COMMAND_NAME;
    }

    @Override
    protected void applyAt(CellRef cellRef, Context context, Size resultSize) {
        XSSFRow row = getSheet().getRow(cellRef.getRow());
        if (row != null) {
            XSSFCell cell = row.getCell(cellRef.getCol());
            if (cell != null) {
                double height = Double.parseDouble(this.height);
                double oldHeight = row.getHeightInPoints();
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Old height: " + oldHeight);
                    LOGGER.debug("Trying to set height: " + height);
                }
                if (height > oldHeight) {
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("New height: " + height);
                    }
                    row.setHeightInPoints((float) height);
                }
            }
        }
    }
}
