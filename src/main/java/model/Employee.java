package model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Employee {

    @Getter
    @Setter
    private String name;
}
