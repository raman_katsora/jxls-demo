import command.RowHeightDemoCommand;
import model.Employee;
import org.jxls.builder.xls.XlsCommentAreaBuilder;
import org.jxls.common.Context;
import org.jxls.transform.poi.PoiTransformer;
import org.jxls.util.JxlsHelper;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class Runner {

    private static final String TEMPLATE = "template.xlsx";
    private static final String OUTPUT = "target/result.xlsx";

    static {
        XlsCommentAreaBuilder.addCommandMapping(RowHeightDemoCommand.COMMAND_NAME, RowHeightDemoCommand.class);
    }

    public static void main(String[] args) throws Exception {
        List<Employee> employees = generateSampleEmployeeData();
        try (InputStream is = Runner.class.getResourceAsStream(TEMPLATE);
             OutputStream os = new FileOutputStream(OUTPUT)) {
            Context context = PoiTransformer.createInitialContext();
            context.putVar("employees", employees);
            JxlsHelper.getInstance().processTemplate(is, os, context);
        }
    }

    private static List<Employee> generateSampleEmployeeData() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Elsa"));
        employees.add(new Employee("Oleg"));
        employees.add(new Employee("Neil"));
        employees.add(new Employee("Maria"));
        employees.add(new Employee("John"));
        return employees;
    }
}
